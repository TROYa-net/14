FROM debian:9 as build

RUN apt update && apt install -y wget gcc make libpcre3 libpcre3-dev zlib1g-dev zlib1g vim curl procps
RUN wget http://nginx.org/download/nginx-1.2.0.tar.gz  && tar xvfz nginx-1.2.0.tar.gz  && cd nginx-1.2.0 && ./configure && make && make install
RUN apt-get clean

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
COPY nginx.conf /usr/local/nginx/conf/nginx.conf
CMD ["./nginx", "-g", "daemon off;"]


